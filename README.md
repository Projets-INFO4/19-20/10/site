# Pages de documentation de STM32Python

## Getting Started
* [Install][] Jekyll

### Start locally
```
git clone git@gricad-gitlab.univ-grenoble-alpes.fr:Projets-INFO4/19-20/10/site.git
git checkout -t origin/grid_card
bundle update
bundle install
sudo gem install ffi -v '1.12.2' --source 'https://rubygems.org/'
bundle install
bundle exec jekyll serve
```

Browse http://127.0.0.1:4000/docsy-jekyll/


### Deploy

Push your repository and changes to GitLab.

## Troubleshooting

[ci]: https://about.gitlab.com/gitlab-ci/
[Jekyll]: http://jekyllrb.com/
[install]: https://jekyllrb.com/docs/installation/
[documentation]: https://jekyllrb.com/docs/home/
[userpages]: https://docs.gitlab.com/ce/user/project/pages/introduction.html#user-or-group-pages
[projpages]: https://docs.gitlab.com/ce/user/project/pages/introduction.html#project-pages
