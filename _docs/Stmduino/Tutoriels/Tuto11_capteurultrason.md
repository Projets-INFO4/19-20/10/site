---
title: Tutoriel Capteur Ultra Son

---

# Tutoriel d'utilisation du capteur d'ultrasons avec Arduino

**- Prérequis :**




**- Le capteur d'ultrasons (Ultrasonic Ranger):**

Ce capteur permet de mesure des distances précisement via des ultrasons. Un transducteur à ultrasons émet des ultrasons qui rebondissent ensuite sur les parois, un deuxième transducteur récepteur reçoit  ces ondes. Il est alors possible de calculer la distances parcourue par les ultrasons. On peut mesurer des distances de quelques centimètres à un ou deux mètres.
Ce capteur est semblable aux capteur de recule d'une voiture.


![Image](../img_tuto_markdown/stmduino/11_ultrason/capteur_ultrason.png)

 *T --> transducteur émetteur (transmetteur)*  |  *R --> transducteur récepteur*
