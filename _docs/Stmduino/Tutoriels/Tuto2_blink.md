---
title: Tutoriel Blink
---

# EXEMPLE BLINK

Dans cet exemple, nous voulons faire clignoter une LED utilisateur de votre STM32.
Une fois Arduino IDE configuré pour votre STM32,

Allez dans Fichier > Exemples > 01.Basics > Blink

![Image](../img_tuto_markdown/stmduino/2_led_clignote/1.png)

Le programme permettant de faire clignoter une LED sur votre STM32 va s'ouvrir.

![Image](../img_tuto_markdown/stmduino/2_led_clignote/2.png)

Cliquez sur le bouton "Téléverser" (représenté par une flèche allant de gauche a droite)

![Image](../img_tuto_markdown/stmduino/2_led_clignote/3.png)

Une LED sur votre STM32 devrait clignoter, dans notre exemple (STM32 F446RE) il s'agit de "LED2", une LED de couleur verte.
