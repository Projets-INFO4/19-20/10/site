---
title: Tutoriel Firmata
---

# EXEMPLE FIRMATA

Dans cet exemple, nous voulons utiliser l'outil Firmata,
Firmata et un outil permettant de configurer graphiquement les ports GPIO (ports d'entrées/sorties) de votre carte.

Fermez la console série.
Ouvrez l'exemple nommé Firmata en faisant : Fichier > Exemples > Firmata > StandardFirmata
Puis téléverser le programme comme réalisé précédemment.

![Image](../img_tuto_markdown/stmduino/2_firmata/1.png)

Télécharger l’exécutable du client Firmata (correspondant à votre système d'exploitation) à cette adresse :

http://firmata.org/wiki/Main_Page

Cliquez sur l’exécutable (pas d'installation requise), le client Firmata s'ouvre :

Cliquez sur Port et choisissez le port COM correspondant à votre périphérique.

![Image](../img_tuto_markdown/stmduino/2_firmata/2.png)

Redémarrer votre STM32 en appuyant sur le bouton poussoir de couleur noir.
Une LED devrait clignoter rapidement sur votre STM32 juste après l'avoir redémarré,
cela veut dire que Firmata est prêt a être utilisé !

vous allez voir apparaître sur le client Firmata, les entrées/sorties pouvant être configurées sur votre carte.

![Image](../img_tuto_markdown/stmduino/2_firmata/3.png)

Cet exemple est terminé.
