---
title: Stmduino
description: Comment installer Stmduino
---
# Installation de Stmduino

## Installation de l'environnement Arduino
Dans un premier temps il est nécessaire d'utiliser l'environnement de dévellopement Arduino.


Télécharger et installer Arduino IDE depuis le lien ci-dessous :
https://www.arduino.cc/en/Main/Software

### Installer le package STM32
Une fois Arduino IDE installé :
Lancez Arduino IDE puis allez dans Fichier > Préférences
Une fenêtre s'ouvre :

![Image](../img_tuto_markdown/stmduino/1_installation/1.png)


Dans "URL de gestionnaire de cartes suplémentaires", entrez l'URL suivante :
https://raw.githubusercontent.com/stm32duino/BoardManagerFiles/master/STM32/package_stm_index.json

Entrez par la suite "OK"

Ensuite : "Outils" > "Type de cartes :" > "Gestionnaire de cartes"
![Image](../img_tuto_markdown/stmduino/1_installation/2.png)

Entrez dans la barre de recherche "STM32" ou "stm" et télécharger le package.
![Image](../img_tuto_markdown/stmduino/1_installation/3.png)

Cliquez sur installer.
![Image](../img_tuto_markdown/stmduino/1_installation/4.png)

### Telecharger et installer STM32CubeIDE

Pour pouvoir utiliser votre STM32 avec l"environnement Arduino, vous devez installer l'outil STM32CubeIDE.
Telecharger et installer STM32CubeIDE disponible a cette adresse :
https://www.st.com/en/development-tools/stm32cubeide.html

Attention : il est nécessaire de créer un compte my.st.com
### --------------------------------------------


Lancez arduino IDE avec un STM32 branché en USB sur l'ordinateur.

Allez dans Outils > "Type de cartes :" et choisissez STM32 Nucleo 64 comme le montre l'image ci-dessous.

![Image](../img_tuto_markdown/stmduino/1_installation/5.png)

Allez ensuite dans Outils > "Board Part Number :" et choisissez la carte STM32 correspondante
à votre materiel (dans notre exemple la Nucleo F446RE).

![Image](../img_tuto_markdown/stmduino/1_installation/6.png)

Dans Outils > "Upload method :"

![Image](../img_tuto_markdown/stmduino/1_installation/7.png)

Ensuite : Outils > "Port: et choisissez le port COM correspondant a votre périphérique STM32.

![Image](../img_tuto_markdown/stmduino/1_installation/8.png)


Vous pouvez alors programmer votre STM32

Félicitation !
Ce tutoriel est términé.
