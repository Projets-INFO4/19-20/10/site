---
title: Tutoriel Capteur de mouvement
---

# Tutoriel d'utilisation du capteur de mouvement  *(PIR motion sensor)* en MicroPython avec une carte STM32WB55

- **Prérequis :**

Le kit de capteur fournis un *shield* (image ci-dessous). Il servira à connecter un capteur à la carte.
Il suffit de le connecter à la carte.
 Les pins Dx permettent de traiter un signal digital (0 ou 1) or les pins Ax gèrent les signaux analogiques.

![Image](../img_tuto_markdown/micropython/shield.png)        ![Image](../img_tuto_markdown/micropython/shield_carte.png)


Pour écrire et exécuter un script MicroPython, rendez vous dans le fichier main.py se trouvant dans le lecteur PYBFLASH de votre carte :

![Image](../img_tuto_markdown/micropython/pybflash.png)

Le fichier main.py sera exécuté par défault au démarrage de MicroPython. Ouvrez ce fichier avec un éditeur de script (ex: Putty).

Pour ce capteur il faudra le brancher sur le **pin D4**.

- **Capteur de mouvement (PIR motion sensor) :**

Le capteur que nous allons utiliser ensemble est un capteur capable de détecter les mouvements. Il détecte les rayonnements infrarouge dans son champ de vision et en déduit une présence ou un mouvement. Ce capteur peut servir dans de nombreuses applications notamment dans un système d'alarme *(Cf Tuto Alarme)*.

![Image](../img_tuto_markdown/micropython/pir_motion_sensor.png)

Pour le code, lorsque le capteur détecte un mouvement, une interruption est déclenchée. Le message "Motion detected" sera afficher dans l'interpréteur Python et la LED de la carte s'allumera pendant un laps de temps. Puis on retourne dans l'état initial.

Code MicroPython :
```python
from machine import Pin
from time import sleep

motion = False

def handle_interrupt(pin):
  global motion
  motion = True
  global interrupt_pin
  interrupt_pin = pin

led = Pin('LED', Pin.OUT)
pir = Pin('D4', Pin.IN)

pir.irq(trigger=Pin.IRQ_RISING, handler=handle_interrupt)

while True:
  if motion:
    print('Motion detected!')
    #print('Motion detected! Interrupt caused by:', interrupt_pin)
    led.value(1)
    sleep(5)
    led.value(0)
    print('Motion stopped!')
    motion = False

```
