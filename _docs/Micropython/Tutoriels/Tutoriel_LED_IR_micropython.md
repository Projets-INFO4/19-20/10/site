---
title: Tutoriel Led Infrarouge
---

# Tutoriel d'utilisation d'une LED infrarouge en MicroPython avec une carte STM32WB55

Ce tuto fonctionne pour tout type de LED mais nous prenons ici l'exemple d'une led infrarouge.

- **Prérequis :**

Le kit de capteur fournis un *shield* (image ci-dessous). Il servira à connecter un capteur à la carte.
Il suffit de le connecter à la carte.
Brancher la LED au shield sur le pin D4. Les pins Dx permettent de traiter un signal digital (0 ou 1) or les pins Ax gèrent les signaux analogiques.

![Image](../img_tuto_markdown/micropython/shield.png)        ![Image](../img_tuto_markdown/micropython/shield_carte.png)


Pour écrire et exécuter un script MicroPython, rendez vous dans le fichier main.py se trouvant dans le lecteur PYBFLASH de votre carte :

![Image](../img_tuto_markdown/micropython/pybflash.png)

Le fichier main.py sera exécuté par défault au démarrage de MicroPython. Ouvrez ce fichier avec un éditeur de script (ex: Putty).

- **Faire clignoter la LED IR :**


Le code proposé est très simple et consiste à faire clignoter notre led en boucle après une initialisation du pin D4.

*Remarque*: Si la led utilisé est une led infrarouge, il est possible de vérifier qu'elle clignote bien avec la caméra de votre téléphone !

Code MicroPython :
```python
import pyb
from pyb import Pin
import time

led = Pin('D4', Pin.OUT_PP)

while True :
	time.sleep_ms(500)      # sleep for 500 milliseconds
	led.off()
	print("LED OFF")
	time.sleep(1)           # sleep for 1 second
	led.on()
	print("LED ON")

```
