---
title: Tutoriel Buzzer
---

# Tutoriel d'utilisation du capteur Buzzer en MicroPython avec une carte STM32WB55

**Même prérequis que pour le tilt-sensor.**

Le capteur est à brancher sur le pin D4.

**Le Buzzer :**

Le buzzer vibre et produit un son lorsque on lui transmet une tension. Il est possible de modifier la fréquence du son.

![Image](../img_tuto_markdown/micropython/buzzer.png)

Le code consite à faire lire au buzzer le tableau *frequency* contenant une gamme de notes.

Code MicroPython :
```python
from pyb import Pin, Timer

frequency = [262, 294, 330, 349, 370, 392, 440, 494]

BUZZER = Pin('D4') # D4 has TIM3, CH2
while true :
    for i in range (0,7) :
        tim3 = Timer(3, freq=frequency[i])
        ch2 = tim3.channel(2, Timer.PWM, pin=BUZZER)
        ch2.pulse_width_percent(5)

```
