---
title: Tutoriel Afficheurs 7 segments
---
# Tutoriel Afficheur 7-segments

Pour utiliser l'afficheur, vous aurez besoin des bibliothèque **pyb** et **tm1637** .
pyb est déjà présente, il vous faut tm1637. Déplacez le fichier **tm1637.py** dans le dossier où se trouve **main.py**.
La bibliothèque est installée ! Vous pouvez à présent les utiliser avec :
```
from pyb import Pin
import tm1637
```
A présent, vous allez définir votre accès à l'afficheur sur A0 de la carte shield avec l'instruction suivante :

```
tm = tm1637.TM1637(clk=Pin('A0'), dio=Pin('A1'))
```

Vous pouvez maintenant modifier l'affichage en utilisant :
```
tm.write([<case1>, <case2>, <case3>, <case4>])
```
Vous contrôler l'affichage des 7 segments dépendamment avec un mot binaire de la forme 0bABCDEFG.  
Mettez simplement un 1 ou 0 selon si vous voulez allumer ou éteindre le segment.  
Par exemple, pour afficher 0123, écrivez :  
```
tm.write([0b0111111, 0b0000110, 0b1011011, 0b1001111])
```

![](../img_tuto_markdown/micropython/300px-7_segment_display_labeled.jpg)


------
Image : https://fr.wikipedia.org/wiki/Fichier:7_segment_display_labeled.svg
