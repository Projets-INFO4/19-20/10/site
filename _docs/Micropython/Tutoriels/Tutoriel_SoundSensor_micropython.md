---
title: Tutoriel Sound-Sensor
---

# Tutoriel d'utilisation du capteur Sound-sensor en MicroPython avec une carte STM32WB55

- **Prérequis :**

Le kit de capteur fournis un *shield* (image ci-dessous). Il servira à connecter un capteur à la carte.
Il suffit de le connecter à la carte.
Brancher ce capteur au shield sur le pin D4. Les pins Dx permettent de traiter un signal digital (0 ou 1) or les pins Ax gèrent les signaux analogiques.

![Image](../img_tuto_markdown/micropython/shield.png)        ![Image](../img_tuto_markdown/micropython/shield_carte.png)


Pour écrire et exécuter un script MicroPython, rendez vous dans le fichier main.py se trouvant dans le lecteur PYBFLASH de votre carte :

![Image](../img_tuto_markdown/micropython/pybflash.png)

Le fichier main.py sera exécuté par défault au démarrage de MicroPython. Ouvrez ce fichier avec un éditeur de script (ex: Putty).


Pour ce capteur il faudra le brancher sur le **pin A1**.

- **Capteur de son :**

![Image](../img_tuto_markdown/micropython/capteur_sonore.png)


Ce capteur peut être utilisé comme détecteur de niveau sonore, en effet sa sortie est proportionnelle au niveau sonore environnant.

Code MicroPython :
```python
import pyb
import time
from pyb import Pin

adc=ADC(PIN('A1'))

while True :
    time_sleep_ms(500)
    print(adc.read())

```

Une valeur sera affichiée dans l'interpréteur Python toutes les 0.5s.
