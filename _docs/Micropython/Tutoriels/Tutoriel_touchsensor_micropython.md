---
title: Tutoriel Touch Sensor
---

# Tutoriel d'utilisation du capteur touch sensor en MicroPython avec une carte STM32WB55

- **Prérequis :**

Le kit de capteur fournis un *shield* (image ci-dessous). Il servira à connecter un capteur à la carte.
Il suffit de le connecter à la carte.
Brancher ce capteur au shield sur le pin D4. Les pins Dx permettent de traiter un signal digital (0 ou 1) or les pins Ax gèrent les signaux analogiques.

![Image](../img_tuto_markdown/micropython/shield.png)        ![Image](../img_tuto_markdown/micropython/shield_carte.png)


Pour écrire et exécuter un script MicroPython, rendez vous dans le fichier main.py se trouvant dans le lecteur PYBFLASH de votre carte :

![Image](../img_tuto_markdown/micropython/pybflash.png)

Le fichier main.py sera exécuté par défault au démarrage de MicroPython. Ouvrez ce fichier avec un éditeur de script (ex: Putty).


- **Capteur tactile (Touch sensor):**

Ce capteur ne possède que 3 broches qu'il faut connecter avec des cables sur la carte shield de façon à ce que GND et VCC (rouge et noir) correspondent bien. Le cable jaune (branché à la broche SIG) sera connecté à D4.

![Image](../img_tuto_markdown/micropython/capteur_tactile.png)

Ce capteur a un fonctionnement annalogue au capteur *tiltsensor*.

Voici le code en MicroPython:
```python

import pyb
import time
from pyb import Pin

p_in = Pin('D4', Pin.IN, Pin.PULL_UP)


while True :
	time.sleep_ms(500)      # sleep for 500 milliseconds

	print(p_in.value()) # get value, 0 or 1

	if(p_in.value() == 1):
		print("ON")
	else:
		print("OFF")
```
