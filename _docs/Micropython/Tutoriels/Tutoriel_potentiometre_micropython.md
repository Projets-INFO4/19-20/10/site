---
title: Tutoriel Potentiomètre
---

# Tutoriel d'utilisation du potentiometre en MicroPython avec une carte STM32WB55

- **Prérequis :**

Le kit de capteur fournis un *shield* (image ci-dessous). Il servira à connecter un capteur à la carte.
Les pins Dx permettent de traiter un signal digital (0 ou 1) or les pins Ax gèrent les signaux analogiques.

![Image](../img_tuto_markdown/micropython/shield.png)        ![Image](../img_tuto_markdown/micropython/shield_carte.png)


Pour écrire et exécuter un script MicroPython, rendez vous dans le fichier main.py se trouvant dans le lecteur PYBFLASH de votre carte :

![Image](../img_tuto_markdown/micropython/pybflash.png)

Le fichier main.py sera exécuté par défault au démarrage de MicroPython. Ouvrez ce fichier avec un éditeur de script (ex: Putty).

Pour ce capteur il faudra le brancher sur le **pin A1**.

- **Le potentiomètre :**

![Image](../img_tuto_markdown/micropython/potentiometre.png)

Le potentiomètre, *rotary angle sensor* en anglais, renvoi une valeur comprise entre 0 et 4095. Il permet de moduler manuellement des valeurs à utiliser (par exemple pour faire varier l'instensité sonore d'un buzzer).


Code MicroPython :
```python
import pyb
from pyb import Pin, ADC
import time

adc = ADC(Pin('A1'))
while(True):
	print(adc.read()) # read value, 0-4095
	time.sleep(1)      # sleep for 500 milliseconds

````
