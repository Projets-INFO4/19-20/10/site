---
title: Tutoriel Tilt Sensor
---

# Tutoriel d'utilisation du capteur tilt-sensor en MicroPython avec une carte STM32WB55

- **Prérequis :**

Le kit de capteur fournis un *shield* (image ci-dessous). Il servira à connecter un capteur à la carte.
Il suffit de le connecter à la carte.
Les pins Dx permettent de traiter un signal digital (0 ou 1) or les pins Ax gèrent les signaux analogiques.

![Image](../img_tuto_markdown/micropython/shield.png)        ![Image](../img_tuto_markdown/micropython/shield_carte.png)


Pour écrire et exécuter un script MicroPython, rendez vous dans le fichier main.py se trouvant dans le lecteur PYBFLASH de votre carte :

![Image](../img_tuto_markdown/micropython/pybflash.png)

Le fichier main.py sera exécuté par défault au démarrage de MicroPython. Ouvrez ce fichier avec un éditeur de script (ex: Putty).

Pour ce capteur il faudra le brancher sur le **pin D4**.

- **Capteur Tilt-sensor :**

![Image](../img_tuto_markdown/micropython/tiltsensorim.png)

Le tilt-sensor, capteur d'inclinaison en français, est un capteur qui mesure la position d'inclinaison par rapport à la gravité. La bille dans le capteur (influencé par le mouvement du capteur) roule et vient faire contact.
Ce capteur peut donc être sous deux états (*nommé* ON et OFF dans le code).

Code MicroPython :
```python
import pyb
import time
from pyb import Pin

p_in = Pin('D4', Pin.IN, Pin.PULL_UP)


while True :
	time.sleep_ms(500)      # sleep for 500 milliseconds

	print(p_in.value()) # get value, 0 or 1

	if(p_in.value() == 1):
		print("ON")
	else:
		print("OFF")


```
